import React, {useState, useEffect} from 'react';
import styles from '../../styles/App.scss';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  Text,
  Platform,
  SafeAreaView,
  TouchableOpacity,
  View,
} from 'react-native';
import {Input, Button, Image} from 'react-native-elements';
// import DateTimePicker from '@react-native-community/datetimepicker';
// import {Picker} from '@react-native-community/picker';
import {URL_SERVICES} from 'react-native-dotenv';
import {useForm} from 'react-hook-form';
import {years} from '../../../data/data';
import ImagePicker from 'react-native-image-picker';
import Moment from 'moment';

export default function EditarVehiculo(props: any) {
  const {navigation} = props;
  const {vehiculo, setIsReloadData} = navigation.state.params;
  const [isLoading, setLoading] = useState(true);
  const [marcas, setMarcas] = useState(new Array());
  const [showCalendar, setShowCalendar] = useState(false);
  const [fechaCompra, setFechaCompra] = useState(
    vehiculo.fechaCompra ? vehiculo.fechaCompra : null,
  );
  const [referencias, setReferencias] = useState(new Array());
  const {register, handleSubmit, setValue, errors} = useForm({
    defaultValues: {
      kilometraje: vehiculo.kilometraje ? vehiculo.kilometraje + '' : null,
      alias: vehiculo.alias ? vehiculo.alias + '' : null,
      modelo: vehiculo.modelo,
      fechacompra: vehiculo.fechaCompra ? vehiculo.fechaCompra : null,
      marca: vehiculo.marca ? vehiculo.marca.marca : null,
      referencia: vehiculo.marca ? vehiculo.marca.referencia : null,
      color: vehiculo.color ? vehiculo.color : null,
    },
  });
  const [referencia, setReferencia] = useState(
    vehiculo.marca ? vehiculo.marca.referencia : '',
  );
  const [marca, setMarca] = useState('');
  const [urlFoto, setUrlFoto] = useState();

  useEffect(() => {
    fetch(URL_SERVICES + 'marca/getAllUnique')
      .then((response) => response.json())
      .then((json) => {
        //console.log("Respuesta motos ::>", json);
        setMarcas(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, [isLoading]);

  useEffect(() => {
    register(
      {name: 'kilometraje'},
      {
        required: {value: true, message: 'Kilometraje requerido'},
        pattern: {
          value: /^[0-9]*$/,
          message: 'Debe ingresar un valor valido',
        },
        validate: {
          positive: (value) => parseInt(value) > 0,
        },
      },
    );
    register(
      {name: 'alias'},
      {
        required: {value: true, message: 'Alias es requerido'},
      },
    );
    register(
      {name: 'color'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'modelo'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'marca'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'referencia'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'fechacompra'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
  }, [register]);

  //console.log("Vehiculo ::::>", vehiculo.kilometraje);

  const updateVehiculo = async (data: any) => {
    //console.log("User logged");
    console.log('Data to send :::>', data);
    let vehiculoToUdp = {
      alias: data.alias,
      kilometraje: data.kilometraje,
      marca: {
        marca: data.marca,
        referencia: data.referencia,
      },
      usuario: vehiculo.usuario,
      tipoVehiculo: vehiculo.tipoVehiculo,
      fotos: urlFoto ? [urlFoto] : [],
      color: data.color,
      fechacompra: data.fechacompra,
      fechaCompraText: Moment(data.fechacompra).format('d/MM/YYYY'),
      modelo: data.modelo,
    };
    fetch(URL_SERVICES + 'vehiculo/update/' + vehiculo.IdVehiculo, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(vehiculoToUdp),
    })
      .then((response) => {
        response.json();
      })
      .then((json) => {
        console.log('Respuesta de actualizar la moto ::>', json);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setIsReloadData(true);
        navigation.navigate('Vehiculo');
      });
  };

  const loadReferencias = (marca: string) => {
    fetch(URL_SERVICES + 'marca/getAllByMarca/' + marca)
      .then((response) => response.json())
      .then((json) => {
        //console.log("Respuesta motos ::>", json);
        setReferencias(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  };

  const uploadImage = async () => {
    const options = {
      title: ' Muestranos una foto!!',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'De mis fotos!!',
      mediaType: 'photo',
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      permissionDenied: {
        title: 'Permisos denegados',
        text: 'Active los permisos para poder agregar fotos a su vehiculo',
      },
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const {uri, fileName, fileSize, type} = response;

        fetch(URL_SERVICES + 'file/signedURL', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            fileName: fileName,
          }),
        })
          .then((response) => {
            response.json().then((url) => {
              fetch(url, {
                method: 'PUT',
                headers: {
                  'Content-Type': type,
                },
                body: {
                  uri: uri,
                  type: type,
                  name: fileName,
                },
              })
                .then((response: any) => {
                  let url: string = response.url.substring(
                    0,
                    response.url.indexOf('?'),
                  );
                  let key = url.substring(url.lastIndexOf('/') + 1, url.length);
                  setUrlFoto({
                    url: response.url.substring(0, response.url.indexOf('?')),
                    date: new Date().toString(),
                    size: fileSize,
                    type: type,
                    selected: false,
                    validate: false,
                    keynameFile: key,
                    nombreArchivo: fileName,
                  });
                })
                .catch((error) => console.error(error))
                .finally(() => {});
            });
          })
          .catch((error) => console.error(error))
          .finally(() => {});
      }
    });
  };

  //Set Url Foto
  useEffect(() => {
    let MyurlFoto = !(
      vehiculo.fotos &&
      vehiculo.fotos[0] &&
      vehiculo.fotos.length > 0
    )
      ? null
      : vehiculo.fotos[0].url;
    setUrlFoto(MyurlFoto ? {url: MyurlFoto} : MyurlFoto);
  }, []);

  return (
    <SafeAreaView style={styles.scrollContainer}>
      <KeyboardAwareScrollView>
        <View>
          <Text
            style={[
              styles.subheadingSecondary,
              styles.centerText,
              styles.regularMargin,
            ]}>
            Cambia o actualiza los datos de moto
          </Text>
        </View>
        <View style={[styles.containerAddPhoto, styles.regularMargin]}>
          <TouchableOpacity onPress={uploadImage}>
            {!urlFoto ? (
              <Image
                style={styles.addPhoto}
                source={require('./../../../images/addPhotoMoto.png')}
              />
            ) : (
              <View style={[styles.addPhoto, styles.photo]}>
                <Image style={styles.addPhoto} source={{uri: urlFoto.url}} />
              </View>
            )}
          </TouchableOpacity>
        </View>
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Kilometraje"
          placeholder="Kilometraje"
          textContentType="telephoneNumber"
          onChangeText={(text) => setValue('kilometraje', text, true)}
          keyboardType="number-pad"
          defaultValue={vehiculo.kilometraje ? vehiculo.kilometraje + '' : ''}
        />
        {errors.kilometraje && (
          <Text style={styles.inputError}>{errors.kilometraje.message}</Text>
        )}

        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Color"
          placeholder="Color"
          defaultValue={vehiculo.color}
          containerStyle={styles.inputContainer}
          keyboardType="ascii-capable"
          onChangeText={(text) => setValue('color', text, true)}
        />
        {errors.color && (
          <Text style={styles.inputError}>{errors.color.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Nombre"
          placeholder="Nombre de tu moto"
          defaultValue={vehiculo.alias}
          containerStyle={styles.inputContainer}
          onChangeText={(text) => setValue('alias', text, true)}
        />
        {errors.alias && (
          <Text style={styles.inputError}>{errors.alias.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Placa"
          placeholder="Placa"
          editable={false}
          keyboardType="number-pad"
          value={vehiculo.placa}
          containerStyle={styles.inputContainer}
          disabled={true}
        />

        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Fecha compra"
          placeholder="Fecha compra"
          editable={vehiculo.fechaCompra ? false : true}
          containerStyle={styles.inputContainer}
          disabled={vehiculo.fechaCompra ? true : false}
          onFocus={() => {
            setShowCalendar(true);
          }}
          value={fechaCompra ? Moment(fechaCompra).format('DD/MM/YYYY') : ''}
        />
        {errors.fechacompra && (
          <Text style={styles.inputError}>{errors.fechacompra.message}</Text>
        )}

        {/* {showCalendar && (
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={new Date()}
            mode="date"
            is24Hour={true}
            display="default"
            locale="es-ES"
            onChange={(event, selectedDate) => {
              setShowCalendar(Platform.OS === 'ios' ? true : false);
              setValue('fechacompra', selectedDate, true);
              setFechaCompra(selectedDate!);
            }}
          />
        )}
        <Picker
          mode="dropdown"
          style={[styles.input, styles.dropdown]}
          itemStyle={styles.input}
          selectedValue={vehiculo.marca ? vehiculo.marca.marca + '' : ''}
          enabled={vehiculo.marca.IdMarca !== 1 ? false : true}
          onValueChange={(itemValue) => {
            if (marca !== itemValue) {
              setValue('marca', itemValue, true);
              setMarca(itemValue);
              setReferencia('');
              loadReferencias(itemValue);
            }
          }}>
          {marcas.map((marca: any) => {
            return (
              <Picker.Item
                label={marca.marca}
                value={marca.marca}
                key={marca.marca}
              />
            );
          })}
        </Picker>

        {errors.marca && (
          <Text style={styles.inputError}>{errors.marca.message}</Text>
        )} */}
        {/* <Picker
          mode="dropdown"
          style={[styles.input, styles.dropdown]}
          itemStyle={styles.input}
          selectedValue={vehiculo.marca ? vehiculo.marca.referencia + '' : ''}
          enabled={vehiculo.marca.IdMarca !== 1 ? false : true}
          onValueChange={(itemValue) => {
            setValue('referencia', itemValue, true);
            setReferencia(itemValue);
          }}>
          {vehiculo.marca.IdMarca !== 1 &&
            referencias.map((ref: any) => {
              return (
                <Picker.Item
                  label={ref.referencia}
                  value={ref.referencia}
                  key={ref.referencia}
                />
              );
            })}
          <Picker.Item
            label={vehiculo.marca.referencia}
            value={vehiculo.marca.referencia}
            key={vehiculo.marca.referencia}
          />
        </Picker>
        {errors.referencia && (
          <Text style={styles.inputError}>{errors.referencia.message}</Text>
        )}
        <Picker
          mode="dropdown"
          style={[styles.input, styles.dropdown]}
          itemStyle={styles.input}
          selectedValue={vehiculo.modelo ? vehiculo.modelo + '' : ''}
          enabled={vehiculo.modelo ? false : true}
          onValueChange={(itemValue) => {
            setValue('modelo', itemValue, true);
          }}>
          {years.map((year) => {
            return (
              <Picker.Item
                label={year.value}
                value={year.value}
                key={year.value}
              />
            );
          })}
        </Picker>
        {errors.modelo && (
          <Text style={styles.inputError}>{errors.modelo.message}</Text>
        )} */}
        <Button
          title="Guardar"
          onPress={handleSubmit(updateVehiculo)}
          buttonStyle={styles.buttonPrimary}
          titleStyle={styles.buttonText}
        />
        <Button
          title="Cancelar"
          onPress={() => {
            navigation.navigate('Vehiculo');
          }}
          buttonStyle={styles.buttonSecondary}
          titleStyle={styles.buttonText}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
