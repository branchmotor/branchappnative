import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import styles from '../../styles/App.scss';
import {
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Button, Icon, Rating, Image} from 'react-native-elements';
import {URL_SERVICES} from 'react-native-dotenv';
import Loading from '../../components/Loading';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import ImagePicker from 'react-native-image-picker';
import ButtonBranch from '../../components/branch/button';

export default function Usuario(props: any) {
  const {navigation} = props;
  const [user, setUser] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [isReloadData, setReloadData] = useState(false);
  const [facebookUser, setFacebookUser] = useState();
  const [urlFoto, setUrlFoto] = useState();
  const starImage = require('./../../../images/ionic-ios-star.png');

  console.log('Url services ::>', URL_SERVICES);

  let userLogged = auth().currentUser;

  // Handle user state changes
  async function onAuthStateChanged(user: any) {
    setUser(user);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    LoadfacebookUser();
    return subscriber; // unsubscribe on unmount
  }, []);

  useEffect(() => {
    fetch(URL_SERVICES + 'usuario/getByEmail/' + userLogged.email, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setUser(json);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setLoading(false);
      });
    setReloadData(false);
  }, [isReloadData]);

  const joinFacebookAccount = async () => {
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Get the email
    //

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }
    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(
      data.accessToken,
    );

    auth()
      .currentUser?.linkWithCredential(facebookCredential)
      .then((resultado) => {
        console.log('Resltado de asociar cuenta a facebook :::>', resultado);
      })
      .catch((error) => {
        console.log('Error al asociar cuentas :::>', error);
      });
  };

  const LoadfacebookUser = () => {
    if (userLogged) {
      userLogged.providerData.forEach(async (provider) => {
        if (provider.providerId == 'facebook.com') {
          setFacebookUser(provider);
        }
      });
    }
  };
  const uploadImage = async () => {
    const options = {
      title: ' Muestranos una foto!!',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'De mis fotos!!',
      mediaType: 'photo',
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      permissionDenied: {
        title: 'Permisos denegados',
        text: 'Active los permisos para poder agregar fotos a su vehiculo',
      },
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const {uri, fileName, fileSize, type} = response;

        fetch(URL_SERVICES + 'file/signedURL', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            fileName: fileName,
          }),
        })
          .then((response) => {
            response.json().then((url) => {
              fetch(url, {
                method: 'PUT',
                headers: {
                  'Content-Type': type,
                },
                body: {
                  uri: uri,
                  type: type,
                  name: fileName,
                },
              })
                .then((response: any) => {
                  let url: string = response.url.substring(
                    0,
                    response.url.indexOf('?'),
                  );
                  let key = url.substring(url.lastIndexOf('/') + 1, url.length);
                  setUrlFoto({
                    url: response.url.substring(0, response.url.indexOf('?')),
                    date: new Date().toString(),
                    size: fileSize,
                    type: type,
                    selected: false,
                    validate: false,
                    keynameFile: key,
                    nombreArchivo: fileName,
                  });
                })
                .catch((error) => console.error(error))
                .finally(() => {});
            });
          })
          .catch((error) => console.error(error))
          .finally(() => {});
      }
    });
  };

  /*const getName = async () => {
    console.log("Get firstName ::>", userLogged);

  };*/
  return (
    <ImageBackground
      source={require('../../../images/bg_sin_logo.jpg')} //LA IDEA ES QUE EL FONDO SEA LA FOTO DE LA MOTO
      style={[styles.bgLogin, styles.bgProfile]}>
      {isLoading ? (
        <Loading isVisible={true} text="Cargando" />
      ) : (
        <View style={styles.userCard}>
          <View style={styles.bgUserCard}>
            <Image
              source={require('./../../../images/bgProfileCard.png')}
              containerStyle={styles.bgUserCardImage}
            />
            <Text style={[styles.headingPrimary, styles.headingUserCard]}>
              {facebookUser ? facebookUser.displayName : user.firstName}
            </Text>
            <View style={styles.rating}>
              <Rating
                type="custom"
                imageSize={15}
                ratingColor="rgba(255,255,266,.8)"
                readonly={true}
                ratingBackgroundColor="#0396c8"
                ratingImage={starImage}
              />
            </View>
            {/* <View style={styles.userButtons}>
              <ButtonBranch iconName="bell-outline" onPress={() => {}} />
              <ButtonBranch iconName="settings-outline" onPress={() => {}} />
            </View> */}
            <View style={styles.userInfoContainer}>
              <View>
                <View style={styles.userInfoItem}>
                  <Icon
                    name="screen-smartphone"
                    type="simple-line-icon"
                    color="#0396c8"
                    size={25}
                  />
                  <Text style={[styles.bodyText, styles.userInfo]}>
                    {user.celular}
                  </Text>
                </View>
                <View style={styles.userInfoItem}>
                  <Icon
                    name="email-outline"
                    type="material-community"
                    color="#0396c8"
                    size={25}
                  />
                  <Text style={[styles.bodyText, styles.userInfo]}>
                    {user.email}
                  </Text>
                </View>
              </View>
              <View>
                <ButtonBranch
                  iconName="pencil-outline"
                  onPress={() => {
                    navigation.navigate('EditarUsuario', {
                      usuario: user,
                      usuarioFacebook: facebookUser,
                      setReloadData,
                    });
                  }}
                />
                <ButtonBranch iconName="bell-outline" onPress={() => {}} />
              </View>
            </View>
            <View>
              <Button
                title="Conectar con Facebook"
                onPress={joinFacebookAccount}
                titleStyle={{marginLeft: 8}}
                buttonStyle={styles.buttonPrimary}
                titleStyle={styles.buttonText}
                disabled={facebookUser ? true : false}
              />
              <Button
                title="Cerrar sesión"
                onPress={() => {
                  Alert.alert(
                    'Branch',
                    '¿Quieres cerrar sesion en branch?',
                    [
                      {
                        text: 'Cancelar',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      {
                        text: 'Cerrar sesion',
                        onPress: () => {
                          auth()
                            .signOut()
                            .then(() => {
                              navigation.navigate('Auth');
                            })
                            .catch(() => {});
                        },
                      },
                    ],
                    {cancelable: false},
                  );
                }}
                buttonStyle={styles.buttonSecondary}
                titleStyle={styles.buttonText}
              />
            </View>
          </View>
          <View style={[styles.containerAddPhotoUser, styles.regularMargin]}>
            <TouchableOpacity onPress={uploadImage}>
              {!urlFoto ? (
                <View>
                  <Image
                    style={styles.addPhoto}
                    source={require('./../../../images/addPhotoProfile.png')}
                  />
                  <View style={styles.iconAddPhotoUser}>
                    <Icon
                      name="plus"
                      type="material-community"
                      color="#0396c8"
                      size={25}
                    />
                  </View>
                </View>
              ) : (
                <View>
                  <View style={[styles.addPhoto, styles.photo]}>
                    <Image
                      style={styles.addPhoto}
                      source={{uri: urlFoto.url}}
                    />
                  </View>
                  <View style={styles.iconAddPhotoUser}>
                    <Icon
                      name="pencil-outline"
                      type="material-community"
                      color="#0396c8"
                      size={25}
                    />
                  </View>
                </View>
              )}
            </TouchableOpacity>
            {/* {!facebookUser ? (
              <Icon
                name="arrow-up-bold-circle-outline"
                type="material-community"
                color="#517fa4"
                size={80}
              />
            ) : (
              <Image
                resizeMode="cover"
                style={styles.imageCard}
                source={{
                  uri:
                    "https://graph.facebook.com/" +
                    facebookUser.uid +
                    "/picture?height=500",
                }}
              />
            )}
            <Text style={{ fontSize: 18, color: "#ffffff", fontWeight: "bold" }}>
              {facebookUser ? facebookUser.displayName : user.firstName}
            </Text> */}
          </View>
        </View>
      )}
    </ImageBackground>
  );
}
