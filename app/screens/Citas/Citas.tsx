import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import ListCitas from '../../components/citas/ListCitas';
import Loading from '../../components/Loading';
import {URL_SERVICES} from 'react-native-dotenv';
import {SafeAreaView} from 'react-native-safe-area-context';
import {withNavigationFocus} from 'react-navigation';
import styles from './../../styles/App.scss';

const CitasScreen = function (props) {
  const {navigation, isFocused} = props;
  const {etapa} = navigation.state.params;
  const [isLoading, setLoading] = useState(true);
  const [citas, setListCitas] = useState(new Array());
  const [isReloadData, setReloadData] = useState(false);

  console.log('Navigation ', navigation.state.params);

  console.log('Url services ::>', URL_SERVICES);

  const user = auth().currentUser;

  const urlws = () => {
    switch (etapa) {
      case 'Pasadas':
        return URL_SERVICES + 'cita/getPasadasByIdUsuario/' + user.uid;
      case 'Activas':
        return URL_SERVICES + 'cita/getActivasByIdUsuario/' + user.uid;
      case 'Futuras':
        return URL_SERVICES + 'cita/getFuturasByIdUsuario/' + user.uid;
      default:
        return URL_SERVICES + 'cita/getFuturasByIdUsuario/' + user.uid;
    }
  };

  useEffect(() => {
    fetch(urlws())
      .then((response) => response.json())
      .then((json) => {
        //console.log("Respuesta motos ::>", json);
        setListCitas(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
    setReloadData(false);
  }, [isFocused]);

  return (
    <SafeAreaView
      style={[styles.container, styles.datesContainer, {paddingTop: 1, width:'100%'}]}>
      {isLoading ? (
        <Loading isVisible={true} text="Cargando" />
      ) : (
        <ListCitas
          style={[styles.container, styles.content]}
          citas={citas}
          navigation={navigation}
          setIsReloadData={setReloadData}
          etapa={etapa}
        />
      )}
    </SafeAreaView>
  );
};

export default withNavigationFocus(CitasScreen);
