import React, {useState, useEffect} from 'react';
import styles from '../../styles/App.scss';
import {View, SafeAreaView, Text, Platform} from 'react-native';
import {Input, Button, Image, Icon} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
// import {Picker} from '@react-native-community/picker';
// import DateTimePicker from '@react-native-community/datetimepicker';
import {URL_SERVICES} from 'react-native-dotenv';
import {useForm} from 'react-hook-form';
import Moment from 'moment';
import {services} from '../../../data/data';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ListServicios from 'app/components/servicios/ListServicios';

export default function AgregarCita(props: any) {
  const {navigation} = props;
  const {setIsReloadData} = navigation.state.params;
  const [vehiculos, setVehiculos] = useState(new Array());
  const {listaServicios} = useState(services);
  const [isLoading, setLoading] = useState(true);
  const {register, handleSubmit, setValue, errors, getValues} = useForm();
  // const [showCalendar, setShowCalendar] = useState(false);
  // const [showTime, setShowTimeCalendar] = useState(false);
  const [fechaCita, setFechaCita] = useState(new Date());
  const [horaCita, setHoraCita] = useState(new Date());
  const [vehiculoSelect, setVehiculoSelect] = useState(null);
  const [servicioSelect, setServicioSelected] = useState('');

  const user = auth().currentUser;

  useEffect(() => {
    fetch(URL_SERVICES + 'vehiculo/getByIdUsuario/' + user?.uid)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        //console.log("Respuesta motos ::>", json);
        setVehiculos(json);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    register(
      {name: 'placa'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'fechacita'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'horacita'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'servicio'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
  }, [register]);

  const createCita = async (data: any) => {
    let vehiculo = vehiculos.find((vehiculo) => {
      if (vehiculo.placa == data.placa) {
        return vehiculo;
      }
    });
    const citaCreate = {
      placa: data.placa,
      taller: vehiculo.taller.IdTaller, //TODO: Taller del vehiculo
      fechaCita: Moment(data.fechacita).format('DD/MM/YYYY'),
      horaCita: Moment(data.horacita).format('HH:mm'),
      servicio: data.servicio,
      estado: 'Solicitada',
    };

    console.log('Data cita :::>', citaCreate);

    fetch(URL_SERVICES + 'cita/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(citaCreate),
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log('Respuesta de crear la cita ::>', json);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setIsReloadData(true);
        navigation.navigate('Citas');
      });
  };

  const selectVehiculo = (placa: String) => {
    let vehiculo = vehiculos.find((vehiculo) => {
      if (vehiculo.placa == placa) {
        return vehiculo;
      }
    });
    setVehiculoSelect(vehiculo);
  };
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isHourPickerVisible, setHourPickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const showHourPicker = () => {
    setHourPickerVisibility(true);
  };
  const hideHourPicker = () => {
    setHourPickerVisibility(false);
  };

  const handleDate = (selectedDate) => {
    setValue('fechacita', selectedDate, true);
    setFechaCita(selectedDate!);
    // console.log("A date has been picked: ", selectedDate);
    hideDatePicker();
  };
  const handleHour = (selectedDate) => {
    setValue('horaCita', selectedDate, true);
    setHoraCita(selectedDate!);
    // console.log("An hour has been picked: ", selectedDate);
    hideHourPicker();
  };

// ESTE FUE MI INTENTO PARA TRAER LAS PLACAS DE LOS VEHÍCULOS PERO ME DA ERROR AL MOSTRAR EL ARRAY QUE DEVUELVE LA FUNCIÓN, SIGO INTENTANDO APRENDER
  const placasVehiculos = vehiculos.map(() =>
    vehiculos[0].placa
  );
  console.log(placasVehiculos)
// ESTE FUE MI INTENTO PARA TRAER LAS PLACAS DE LOS VEHÍCULOS PERO ME DA ERROR AL MOSTRAR EL ARRAY QUE DEVUELVE LA FUNCIÓN, SIGO INTENTANDO APRENDER
// CON ESTO LA APP DE ANDROID SE CRASHEA PERO LA DE IOS SI MUESTRA EL ERROR

  return (
    <SafeAreaView>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}
        extraScrollHeight={150}
        enableOnAndroid={true}
        keyboardShouldPersistTaps="handled">
        <Text style={[styles.headingSecondary, styles.centerText]}>
          Estamos felices de poder ayudarte
        </Text>
        <View style={[styles.containerAddDatePhoto, styles.regularMargin]}>
          <Image
            style={styles.addDatePhoto}
            source={require('../../../images/addDate.png')}
          />
        </View>
        <Text
          style={[
            styles.subheadingSecondary,
            styles.centerText,
            styles.regularMargin,
          ]}>
          Agenda tu cita llenando estos datos
        </Text>
        {/* <Picker
          mode="dropdown"
          style={[styles.input, styles.dropdown]}
          itemStyle={styles.input}
          selectedValue={getValues('placa')}
          onValueChange={(itemValue, itemIndex) => {
            selectVehiculo(itemValue);
            setValue('placa', itemValue, true);
          }}>
          {vehiculos.map((vehiculo) => {
            return (
              <Picker.Item
                label={vehiculo.placa}
                value={vehiculo.placa}
                key={vehiculo.placa}
              />
            );
          })}
        </Picker> */}
        <Text style={[styles.bodyTextBold, styles.dropdownLabelAndroid]}>
             Selecciona una moto para el servicio 
        </Text>
        <RNPickerSelect
          placeholder={{
            label: 'Selecciona una opción',
            color: "#0396c8",
          }}
          useNativeAndroidPickerStyle={false}
          style={{
            inputAndroid: {
              width:"95%",
              marginLeft: "auto",
              marginRight: "auto",
              padding:10,
              fontFamily:"IBMPlexSans-Light",
              fontSize:18,
              borderBottomColor:'gray',
              borderBottomWidth:1,
              marginBottom:20,
            },
            inputIOS: {
              width:"95%",
              height:60,
              marginLeft: "auto",
              marginRight: "auto",
              padding:10,
              fontFamily:"IBMPlexSans-Light",
              fontSize:18,
              borderBottomColor:"gray",
              borderBottomWidth:1,
              marginBottom:20,
            },
            iconContainer: {
              top: 10,
              right: 12,
            },
          }}
          Icon={() => {
            return <Icon name={'chevron-down'}
            color="#00aced"
            type="material-community" />;
          }}
          onValueChange={(itemValue, itemIndex) => {
            selectVehiculo(itemValue);
            setValue('servicio', itemValue, true);
          }}
          items={[
            {label: placasVehiculos, value: placasVehiculos}
          ]} 
        />
        {errors.placa && (
          <Text style={styles.inputError}>{errors.placa.message}</Text>
        )}
        {vehiculoSelect && !vehiculoSelect.taller && (
          <Text
            style={[
              styles.subheadingSecondary,
              styles.centerText,
              styles.regularMargin,
            ]}>
            No se puede agendar citas a esta vehiculo
          </Text>
        )}

        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Fecha"
          placeholder="Fecha"
          keyboardType="ascii-capable"
          rightIcon={
            <Icon
              onPress={showDatePicker}
              name={'calendar-edit'}
              color="#00aced"
              type="material-community"
            />
          }
          value={Moment(fechaCita).format('DD/MM/YYYY')}
        />
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          mode="date"
          onConfirm={handleDate}
          onCancel={hideDatePicker}
        />
        {errors.fechacita && (
          <Text style={styles.inputError}>{errors.fechacita.message}</Text>
        )}
        <Input
          labelStyle={styles.label}
          inputStyle={styles.input}
          label="Hora"
          placeholder="Hora"
          keyboardType="ascii-capable"
          rightIcon={
            <Icon
              onPress={showHourPicker}
              name={'alarm-multiple'}
              color="#00aced"
              type="material-community"
            />
          }
          value={Moment(horaCita).format('hh:mm a')}
        />
        <DateTimePickerModal
          isVisible={isHourPickerVisible}
          mode="time"
          onConfirm={handleHour}
          onCancel={hideHourPicker}
        />
        {errors.horacita && (
          <Text style={styles.inputError}>{errors.horacita.message}</Text>
        )}
        {/* <Picker
          mode="dropdown"
          style={[styles.input, styles.dropdown]}
          itemStyle={styles.input}
          selectedValue={servicioSelect}
          onValueChange={(itemValue, itemIndex) => {
            setServicioSelected(itemValue);
            setValue('servicio', itemValue, true);
          }}>
          {services.map((servicio) => {
            return (
              <Picker.Item
                label={servicio.value}
                value={servicio.value}
                key={servicio.value}
              />
            );
          })}
        </Picker> */}
        {/* {services.map((servicio) => {
          return( */}
        <Text style={[styles.bodyTextBold, styles.dropdownLabelAndroid]}>
             Selecciona el tipo de servicio 
        </Text>
        <RNPickerSelect
          placeholder={{
            label: 'Selecciona una opción',
            color: "#0396c8",
          }}
          useNativeAndroidPickerStyle={false}
          style={{
            inputAndroid: {
              width:"95%",
              marginLeft: "auto",
              marginRight: "auto",
              padding:10,
              fontFamily:"IBMPlexSans-Light",
              fontSize:18,
              borderBottomColor:'gray',
              borderBottomWidth:1,
              marginBottom:20,
            },
            inputIOS: {
              width:"95%",
              height:60,
              marginLeft: "auto",
              marginRight: "auto",
              padding:10,
              fontFamily:"IBMPlexSans-Light",
              fontSize:18,
              borderBottomColor:"gray",
              borderBottomWidth:1,
              marginBottom:20,
            },
            iconContainer: {
              top: 10,
              right: 12,
            },
          }}
          Icon={() => {
            return <Icon name={'chevron-down'}
            color="#00aced"
            type="material-community" />;
          }}
          onValueChange={(itemValue, itemIndex) => {
            setServicioSelected(itemValue);
            setValue('servicio', itemValue, true);
          }}
          items={[
            // {label: servicio.value, value: servicio.value}
          ]} 
        />
        {errors.servicio && (
          <Text style={styles.inputError}>{errors.servicio.message}</Text>
        )}
        <Button
          title="Agendar cita"
          onPress={handleSubmit(createCita)}
          buttonStyle={styles.buttonPrimary}
          titleStyle={styles.buttonText}
          disabled={vehiculoSelect && !vehiculoSelect.taller ? true : false}
        />
        <Button
          title="Cancelar"
          onPress={() => {
            navigation.navigate('Citas');
            console.log(navigation.navigate('Citas'));
          }}
          buttonStyle={styles.buttonSecondary}
          titleStyle={styles.buttonText}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
