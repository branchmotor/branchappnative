import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';

export default function AuthLoadingScreen(props: any) {
  const {navigation} = props;
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(myuser: any) {
    setUser(myuser);
    if (initializing) {
      setInitializing(false);
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) {
    return null;
  }

  if (!user) {
    return navigation.navigate('Auth');
  } else {
    return navigation.navigate('Main');
  }
}
