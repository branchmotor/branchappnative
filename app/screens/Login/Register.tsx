import React, {useEffect, useState} from 'react';
import styles from '../../styles/App.scss';
import {View, Text, SafeAreaView} from 'react-native';
import {useForm} from 'react-hook-form';
import {Input, Button, Image, Icon} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import {URL_SERVICES} from 'react-native-dotenv';
import Snackbar from 'react-native-snackbar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default function Register(props: any) {
  const {navigation} = props;
  const {register, handleSubmit, setValue, errors} = useForm();
  const [user, setUser] = useState();
  const [togglePassword, passwordState] = useState(true);

  // Handle user state changes
  async function onAuthStateChanged(myuser: any) {
    console.log('user :::>', myuser);
    setUser(myuser);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  useEffect(() => {
    register(
      {name: 'email'},
      {
        required: {value: true, message: 'Email requerido'},
        pattern: {
          value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
          message: 'El valor ingresado no es un email',
        },
      },
    );
    register(
      {name: 'password'},
      {
        required: {value: true, message: 'Password requerido'},
        minLength: {value: 6, message: 'La longitud debe ser mayor a 6'},
      },
    );
    register(
      {name: 'nombre'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'identificacion'},
      {
        required: {value: true, message: 'Campo requerido'},
      },
    );
    register(
      {name: 'celular'},
      {
        required: {value: true, message: 'Campo requerido'},
        minLength: {value: 10, message: 'La longitud debe ser de 10'},
        maxLength: {value: 10, message: 'La longitud debe ser de 10'},
      },
    );
  }, [register]);

  const registrarUsuario = async (data: any) => {
    let usuarioToCreate = {
      firstName: data.nombre,
      email: data.email,
      celular: data.celular,
      identificacion: data.identificacion,
      tipoUsuario: 'Cliente',
      password: data.password,
    };
    fetch(URL_SERVICES + 'usuario/createFireBaseUser', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(usuarioToCreate),
    })
      .then((response) => {
        console.log('Response :::>', response);
        return response.json();
      })
      .then(async (json) => {
        console.log('Error al logear :::>', json);
        const {error} = json;
        if (error) {
          Snackbar.show({
            text: error,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          await auth()
            .signInWithEmailAndPassword(data.email, data.password)
            .then((result) => {
              console.log('Resultado de autenticacion exitoso', result);
              navigation.navigate('Main');
            })
            .catch((error) => {
              var errorCode = error.code;
              var errorMessage = error.message;
              if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
              } else {
                console.log('Error al registrar el usuario', errorMessage);
                Snackbar.show({
                  text: 'Ocurrio un error al actualizar el usario',
                  duration: Snackbar.LENGTH_SHORT,
                });
              }
              console.log(error);
            });
        }
      })
      .catch((error) => console.error(error))
      .finally(() => {});
  };

  return (
    <SafeAreaView style={styles.registerContainer}>
      <KeyboardAwareScrollView
        extraScrollHeight={150}
        enableOnAndroid={true}
        keyboardShouldPersistTaps="handled">
        <View>
          <Text
            style={[
              styles.subheadingSecondary,
              styles.centerText,
              styles.regularMargin,
            ]}>
            Ahora puedes centralizar
          </Text>
          <Text style={[styles.headingSecondary, styles.centerText]}>
            toda la información de tus motos
          </Text>
        </View>
        <View style={[styles.containerAddPhoto, styles.regularMargin]}>
          <Image
            style={styles.userEditImage}
            source={require('./../../../images/userEdit.png')}
          />
        </View>
        <View>
          <Text
            style={[
              styles.subheadingSecondary,
              styles.centerText,
              styles.regularMargin,
            ]}>
            Para empezar rellena estos datos.
          </Text>
        </View>
        <Input
          label="Email"
          placeholder="Email"
          labelStyle={styles.label}
          inputStyle={styles.input}
          onChangeText={(text) => setValue('email', text, true)}
          textContentType="emailAddress"
          keyboardType="email-address"
        />
        {errors.email && (
          <Text style={styles.inputError}>{errors.email.message}</Text>
        )}
        <Input
          label="Contraseña"
          placeholder="Contraseña"
          textContentType="password"
          labelStyle={styles.label}
          inputStyle={styles.input}
          onChangeText={(text) => setValue('password', text, true)}
          secureTextEntry={togglePassword}
          rightIcon={
            <Icon
              onPress={() => {
                passwordState(!togglePassword);
              }}
              name={togglePassword ? 'eye-outline' : 'eye-off-outline'}
              color="#00aced"
              type="material-community"
            />
          }
        />
        {errors.password && (
          <Text style={styles.inputError}>{errors.password.message}</Text>
        )}
        <Input
          label="Nombre Completo"
          placeholder="Nombre Completo"
          onChangeText={(text) => setValue('nombre', text, true)}
          labelStyle={styles.label}
          inputStyle={styles.input}
        />
        {errors.nombre && (
          <Text style={styles.inputError}>{errors.nombre.message}</Text>
        )}
        <Input
          label="Documento"
          placeholder="Documento"
          onChangeText={(text) => setValue('identificacion', text, true)}
          labelStyle={styles.label}
          inputStyle={styles.input}
        />
        {errors.identificacion && (
          <Text style={styles.inputError}>{errors.identificacion.message}</Text>
        )}
        <Input
          label="Celular"
          placeholder="Celular"
          onChangeText={(text) => setValue('celular', text, true)}
          labelStyle={styles.label}
          inputStyle={styles.input}
        />
        {errors.celular && (
          <Text style={styles.inputError}>{errors.celular.message}</Text>
        )}
        <Button
          title="Registrarme"
          onPress={handleSubmit(registrarUsuario)}
          buttonStyle={styles.buttonPrimary}
          titleStyle={styles.buttonText}
        />
        <Button
          title="Cancelar"
          onPress={() => {
            navigation.navigate('MainLogin');
          }}
          buttonStyle={styles.buttonSecondary}
          titleStyle={styles.buttonText}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
}
