import React, {useState, useEffect} from 'react';
import styles from '../styles/App.scss';
import {
  FlatList,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import {Overlay, Image, Icon} from 'react-native-elements';
import GallerySwiper from 'react-native-gallery-swiper';

export default function GalleryList(props: any) {
  const {images, itemsToRender} = props;
  const [visible, setVisible] = useState(false);
  const [initialPage, setInitialPage] = useState(0);

  const loadGallery = (index: number) => {
    setInitialPage(index);
    setVisible(!visible);
  };
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const itemsToShow = !itemsToRender ? 1 : itemsToRender;

  return (
    <View style={styles.viewGallery}>
      <FlatList
        horizontal={false}
        data={images.slice(0, itemsToShow)}
        renderItem={(photo) => (
          <Photo
            image={photo.item}
            loadGallery={loadGallery}
            initialPage={initialPage}
            index={photo.index}
          />
        )}
        keyExtractor={(item) => item.id}
        numColumns="2"
      />
      <OverlayGallery
        photos={images}
        visible={visible}
        toggleOverlay={loadGallery}
        initialPage={initialPage}
      />
    </View>
  );
}

const Photo = (props: any) => {
  const {image, loadGallery, index} = props;

  return (
    <TouchableOpacity
      onPress={() => {
        loadGallery(index);
      }}
      style={styles.photosGallery}>
      <Image
        source={{uri: image.thumbnail}}
        containerStyle={styles.photosGalleryImage}
        resizeMode="cover"
        PlaceholderContent={<ActivityIndicator size="small" style={styles.loadingGallery} />}
      />
    </TouchableOpacity>
  );
};

const OverlayGallery = (props: any) => {
  const {visible, toggleOverlay, photos, initialPage} = props;
  return (
    <Overlay
      isVisible={visible}
      onBackdropPress={toggleOverlay}
      overlayStyle={styles.overlayGallery}
      fullScreen={false}>
      <GallerySwiper
        style={styles.modalGallery}
        images={photos}
        initialPage={initialPage}
        resizeMode="contain"
      />
      <Icon
        containerStyle={styles.icon}
        name='window-close'
        color="#00aced"
        type="material-community"
        onPress={toggleOverlay}
        />
    </Overlay>
  );
};
