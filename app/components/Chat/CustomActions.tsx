import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {URL_SERVICES} from 'react-native-dotenv';
import ImagePicker from 'react-native-image-picker';
import {Icon, Image} from 'react-native-elements';
import styles from '../../styles/App.scss';

export default function CustomActions(props: any) {
  const {onSend} = props;
  const onActionsPress = () => {
    const options = {
      title: ' Muestranos una foto!!',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'De mis fotos!!',
      mediaType: 'photo',
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      permissionDenied: {
        title: 'Permisos denegados',
        text: 'Active los permisos para poder agregar fotos a su vehiculo',
      },
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response: any) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const {uri, fileName, fileSize, type} = response;

        fetch(URL_SERVICES + 'file/signedURL', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            fileName: fileName,
          }),
        })
          .then((response) => {
            return response.json();
          })
          .then((url) => {
            console.log('Url to load :::>', url);
            fetch(url, {
              method: 'PUT',
              headers: {
                'Content-Type': type,
              },
              body: {
                uri: uri,
                type: type,
                name: fileName,
              },
            })
              .then((response: any) => {
                console.log('Response :::>', response);
                let url: string = response.url.substring(
                  0,
                  response.url.indexOf('?'),
                );

                onSend([{image: url}]);
              })
              .catch((error: any) => console.error(error))
              .finally(() => {});
          })
          .catch((error) => console.error(error))
          .finally(() => {});
      }
    });
  };

  return (
    <TouchableOpacity
      style={styles.chatUploadContainer}
      onPress={onActionsPress}>
      <RenderIcon />
    </TouchableOpacity>
  );
}

function RenderIcon(props: any) {
  const {renderIcon} = props;
  if (renderIcon) {
    return renderIcon;
  } else {
    return (
      <View style={styles.iconButton}>
        <Image
          source={require('./../../../images/button.png')}
          style={styles.iconButtonImage}
        />
        <Icon
          containerStyle={styles.iconButtonIcon}
          name="cloud-upload-outline"
          type="material-community"
          color="#0396c8"
          size={25}
        />
      </View>
    );
  }
}
