import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';

import {getLocationAsync, pickImageAsync, takePictureAsync} from './mediaUtils';

export default function AccessoryBar(props: any) {
  const {onSend, isTyping} = props;
  return (
    <View style={styles.container}>
      <Button onPress={() => pickImageAsync(onSend)} name="photo" />
      <Button onPress={() => takePictureAsync(onSend)} name="camera" />
      <Button onPress={() => getLocationAsync(onSend)} name="my-location" />
      <Button
        onPress={() => {
          isTyping();
        }}
        name="chat"
      />
    </View>
  );
}

const Button = ({onPress, size = 30, color = '#0396c8', ...props}) => (
  <TouchableOpacity onPress={onPress}>
    <Icon
      {...props}
      name="plus"
      type="material-community"
      color={color}
      size={25}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    height: 44,
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: '#bde1e1',
  },
});
