import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from '../screens/Login/Login';
import RegisterScreen from '../screens/Login/Register';
import MainLogin from '../screens/Login/MainLogin';

const LoginScreenStacks = createStackNavigator(
  {
    MainLogin: {
      screen: MainLogin,
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: () => ({}),
    },
    Register: {
      screen: RegisterScreen,
      navigationOptions: () => ({}),
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'MainLogin',
  },
);

export default LoginScreenStacks;
