import {createStackNavigator} from 'react-navigation-stack';
import CitaScreen from '../screens/Citas/Citas';
import DetailCitaScreen from '../screens/Citas/DetailCita';
import VerPDFScreen from '../screens/pdf/viewPdf';
import ChatScreen from '../screens/Citas/Chat';
import AgregarCitaScreen from '../screens/Citas/AgregarCita';

const CitasActivasStacks = createStackNavigator(
  {
    Citas: {
      screen: CitaScreen,
      navigationOptions: () => ({}),
      params: {
        etapa: 'Activas',
      },
    },
    DetailCita: {
      screen: DetailCitaScreen,
      navigationOptions: () => ({}),
    },
    VerPDF: {
      screen: VerPDFScreen,
      navigationOptions: () => ({
        title: 'Cotizacion',
      }),
    },
    chat: {
      screen: ChatScreen,
    },
    AgregarCita: {
      screen: AgregarCitaScreen,
    },
  },
  {
    headerMode: 'none',
    navigationOptions: (props) => {
      const {navigation} = props;
      let tabBarVisible = true;
      if (navigation.state.index > 0) {
        tabBarVisible = false;
      }
      return {
        tabBarVisible,
      };
    },
  },
);

export default CitasActivasStacks;
