import React, {useContext} from 'react';
import {Icon, withBadge} from 'react-native-elements';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import VehiculosScreenStacks from './VehiculosStacks';
import TalleresStacks from './TalleresStacks';
import LoginStacks from './LoginStacks';
import CitasStacks from './CitasStacks';
import UsuariosStacks from './UsuariosStacks';
import AuthLoadingScreen from '../screens/Login/AuthLoadingScreen';
import SplasScreen from '../screens/SplashScreen';
import {NotificationContext} from '../components/ContextNotifications';

const NavigationStacks = createBottomTabNavigator(
  {
    //Nuestros stacks de navegacion
    Vehiculos: {
      screen: VehiculosScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: 'Motos',
        tabBarIcon: ({tintColor}) => (
          <Icon
            type="font-awesome"
            name="motorcycle"
            size={22}
            color={tintColor}
          />
        ),
      }),
    },
    Talleres: {
      screen: TalleresStacks,
      navigationOptions: () => {
        return {
          tabBarLabel: 'Talleres',
          tabBarIcon: ({tintColor}) => (
            <BadgeIcon color={tintColor} type="font-awesome" name="wrench" />
          ),
        };
      },
    },
    Usuarios: {
      screen: UsuariosStacks,
      navigationOptions: () => ({
        tabBarLabel: 'Perfil',
        tabBarIcon: ({tintColor}) => (
          <Icon type="font-awesome" name="user" size={22} color={tintColor} />
        ),
      }),
    },
    Citas: {
      screen: CitasStacks,
      navigationOptions: () => ({
        tabBarLabel: 'Citas',
        tabBarIcon: ({tintColor}) => (
          <Icon type="foundation" name="calendar" size={22} color={tintColor} />
        ),
      }),
    },
  },
  {
    lazy: true,
    tabBarOptions: {
      style: {
        backgroundColor: '#0396c8',
        borderRadius: 35,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 3,
        position: 'absolute',
        borderColor: '#5be5e5',
        borderStyle: 'solid',
        borderWidth: 1,
      },
      activeTintColor: '#041c24',
      inactiveTintColor: '#5be5e5',
    },
  },
);

const MainStacks = createSwitchNavigator(
  {
    Splash: SplasScreen,
    AuthLoading: AuthLoadingScreen,
    Auth: LoginStacks,
    Main: NavigationStacks,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const BadgeIcon = (props: any) => {
  const {color, type, name} = props;
  const {value: NumberNotificacions} = useContext(NotificationContext);
  const ShowBadge = NumberNotificacions > 0 ? false : true;
  const BadgedIcon = withBadge(NumberNotificacions, {hidden: ShowBadge})(Icon);
  return <BadgedIcon type={type} name={name} size={22} color={color} />;
};

export default createAppContainer(MainStacks);
