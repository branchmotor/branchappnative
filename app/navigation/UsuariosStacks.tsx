import {createStackNavigator} from 'react-navigation-stack';
import UsuarioScreen from '../screens/Usuario/Usuario';
import EditarUsuarioScreen from '../screens/Usuario/EditarUsuario';

const UsuariosScreenStacks = createStackNavigator(
  {
    Usuario: {
      screen: UsuarioScreen,
      navigationOptions: () => ({
        title: 'Usuario',
      }),
    },
    EditarUsuario: {
      screen: EditarUsuarioScreen,
      navigationOptions: () => ({
        title: 'Editar usuario',
      }),
    },
  },
  {
    initialRouteName: 'Usuario',
    defaultNavigationOptions: {
      headerShown: false,
      // headerStyle: {
      //   backgroundColor: "#041c24",
      // }
      // headerTintColor: "#0396c8",
      // headerTitleAlign: "center",
      // cardStyle: {
      //   backgroundColor: "#d7f8f8",
      // },
    },
  },
);

export default UsuariosScreenStacks;
