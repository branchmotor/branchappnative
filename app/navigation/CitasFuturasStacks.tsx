import {createStackNavigator} from 'react-navigation-stack';
import CitaScreen from '../screens/Citas/Citas';
import DetailCitaScreen from '../screens/Citas/DetailCita';
import VerPDFScreen from '../screens/pdf/viewPdf';
import AgregarCitaScreen from '../screens/Citas/AgregarCita';

const CitasFuturasStacks = createStackNavigator(
  {
    Citas: {
      screen: CitaScreen,
      navigationOptions: () => ({}),
      params: {
        etapa: 'Futuras',
      },
    },
    DetailCita: {
      screen: DetailCitaScreen,
      navigationOptions: () => ({}),
    },
    VerPDF: {
      screen: VerPDFScreen,
      navigationOptions: () => ({
        title: 'Cotizacion',
      }),
    },
    AgregarCita: {
      screen: AgregarCitaScreen,
    },
  },
  {
    headerMode: 'none',
    navigationOptions: (props) => {
      const {navigation} = props;
      let tabBarVisible = true;
      if (navigation.state.index > 0) {
        tabBarVisible = false;
      }
      return {
        tabBarVisible,
      };
    },
  },
);

export default CitasFuturasStacks;
