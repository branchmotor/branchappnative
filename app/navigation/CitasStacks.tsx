import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import CitasPasadasStacks from './CitasPasadasStacks';
import CitasActivasStacks from './CitasActivasStacks';
import CitasFuturasStacks from './CitasFuturasStacks';

const citasStacks = createMaterialTopTabNavigator(
  {
    Activas: {
      screen: CitasActivasStacks,
      navigationOptions: () => ({}),
    },
    Pasadas: {
      screen: CitasPasadasStacks,
      navigationOptions: () => ({}),
    },
    Futuras: {
      screen: CitasFuturasStacks,
      navigationOptions: () => ({}),
    },
  },
  {
    initialRouteName: 'Pasadas',
    order: ['Pasadas', 'Activas', 'Futuras'],
    lazy: true,
    swipeEnabled: false,
    tabBarOptions: {
      style: {
        backgroundColor: '#041c24',
        marginBottom: -20,
      },
      activeTintColor: '#041c24',
      inactiveTintColor: '#5be5e5',
      labelStyle: {
        fontFamily: 'IBMPlexSans-Bold',
      },
      indicatorStyle: {
        borderColor: '#d7f8f8',
        borderWidth: 24,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
      },
    },
  },
);

const CitasScreenStacks = createStackNavigator(
  {
    Citas: citasStacks,
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#041c24',
      },
      headerTintColor: '#0396c8',
      headerTitleAlign: 'center',
      cardStyle: {
        backgroundColor: '#d7f8f8',
      },
      headerTitleStyle: {
        fontFamily: 'IBMPlexSans-Bold',
      },
    },
  },
);

export default CitasScreenStacks;
